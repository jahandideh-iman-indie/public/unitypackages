﻿namespace Arman.Game.InventorySystem.Core
{
    public interface InventoryItemConstraint
    {
        int ApplyTo(int value);
    }
}